# PapilonAndroid

# App Information

If it is to be tested from the emulator, the address 10.0.2.2:5000 should be used.
If testing will be done on a mobile device, the IP address of the computer should be used.

Fixed Location given ConstantHelper.Latitude and  ConstantHelper.Longitude params
If you want to change, you can change these parameters.

install to device /apk/app-debug.apk
run PapilonApi /PapilonApi/app.py

sqlite database is being used /PapilonApi/case.sqlite

Database have a two table(Location&Permission)

# Libraries used 

# Android
Retrofit, OkHTTP, LifeCycle, Room
# Python
flask


