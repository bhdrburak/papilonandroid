import sqlite3

conn = sqlite3.connect("case.sqlite")

cursor = conn.cursor()

sql_query = """ CREATE TABLE permissions (id text PRIMARY KEY, permissionType text NOT NULL, date text NOT NULL)"""

cursor.execute(sql_query)