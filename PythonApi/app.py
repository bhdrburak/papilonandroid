from flask import Flask, request, jsonify
import sqlite3



app = Flask(__name__)

def db_connection():
    conn = None
    try:
        conn = sqlite3.connect("case.sqlite")
    except sqlite3.error as e:
        print(e)
    return conn        


@app.route('/location', methods=['GET', 'POST'])
def location():
    location_list = []
    conn = db_connection()
    cursor = conn.cursor()
    if request.method == 'GET':
        cursor = conn.execute("SELECT * FROM locations")
        for row in cursor:
            new_obj = {'id': row[0],'latitude':row[1],'longitude':row[2],'date':row[3]}
            location_list.append(new_obj)

        if len(location_list) > 0:
            return jsonify(location_list)
        else:
            jsonify(isSuccess=False, message="İşlem başarısız."), 404    

    if request.method == 'POST':
        id = request.json["id"]
        new_lat = request.json["latitude"]
        new_long = request.json["longitude"]
        new_date = request.json["date"]

        sqlite_insert_query = """INSERT or REPLACE INTO locations(id, latitude, longitude, date) VALUES (?, ?, ?, ?)"""

        cursor.execute(sqlite_insert_query, (id, new_lat, new_long, new_date))
        conn.commit()
        return jsonify(isSuccess=True, message="kayıt başarılı."),201



@app.route('/addCachedlocations', methods=['GET', 'POST'])
def locationCached():
    conn = db_connection()
    cursor = conn.cursor()
    if request.method == 'POST':
        sqlite_insert_query = """INSERT or REPLACE INTO locations(id, latitude, longitude, date) VALUES (?, ?, ?, ?)"""

        for items in request.json: 
            id = items["id"]
            new_lat = items["latitude"]
            new_long = items["longitude"]
            new_date = items["date"]
            cursor.execute(sqlite_insert_query, (id, new_lat, new_long, new_date))



        conn.commit()
        return jsonify(isSuccess=True, message="kayıt başarılı."),201


@app.route('/permission', methods=['GET', 'POST'])
def permission():
    conn = db_connection()
    cursor = conn.cursor()
    if request.method == 'POST':
        id = request.json["id"]
        permissionType = request.json["permissionType"]
        date = request.json["date"]

        sqlite_insert_query = """INSERT or REPLACE INTO permissions(id, permissionType, date) VALUES (?, ?, ?)"""

        cursor.execute(sqlite_insert_query, (id, permissionType, date))
        conn.commit()
        return jsonify(isSuccess=True, message="kayıt başarılı."),201


if __name__ == '__main__':
    app.run()

