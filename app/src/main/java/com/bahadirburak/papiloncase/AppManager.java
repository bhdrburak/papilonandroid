package com.bahadirburak.papiloncase;


import android.content.Context;

import com.bahadirburak.papiloncase.webService.RetrofitProvider;
import com.bahadirburak.papiloncase.webService.RetrofitService;


public class AppManager {


    public static RetrofitService retrofitService;
    public static int appRequestAttempt = 0;
    public static Context appContext;


    public static void setServices() {
        retrofitService = RetrofitProvider.createService(RetrofitService.class);
    }
    public static void setAppContext(Context context) {
        appContext = context;
    }

}
