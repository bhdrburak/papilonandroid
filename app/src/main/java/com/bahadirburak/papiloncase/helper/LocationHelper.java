package com.bahadirburak.papiloncase.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;

import com.bahadirburak.papiloncase.BaseApplication;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

public class LocationHelper {

    private Context context;

    public LocationHelper(Context context){
        this.context = context;
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    public static Location updateLastLocation(Location location) {
        Location locationNew = null;
        LocationManager locationManager = (LocationManager) BaseApplication.getContext().getSystemService(BaseApplication.getContext().LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(BaseApplication.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(BaseApplication.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            locationNew = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationNew != null) {
                PreferenceHelper.addString(ConstantHelper.LATITUDE, String.valueOf(locationNew.getLatitude()));
                PreferenceHelper.addString(ConstantHelper.LONGITUDE, String.valueOf(locationNew.getLongitude()));
            } else {
                locationNew = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locationNew != null) {
                    PreferenceHelper.addString(ConstantHelper.LATITUDE, String.valueOf(locationNew.getLatitude()));
                    PreferenceHelper.addString(ConstantHelper.LONGITUDE, String.valueOf(locationNew.getLongitude()));
                }
            }
        }
        return locationNew;
    }

    public static boolean checkLocationForEvent(Location location) {
        Location currentLocation = new Location("");
        currentLocation.setLatitude(ConstantHelper.CURRENT_LOCATION_LATITUDE);
        currentLocation.setLongitude(ConstantHelper.CURRENT_LOCATION_LONGITUDE);
        int distanceToArea = (int) currentLocation.distanceTo(location);
        if (distanceToArea < 100)
            return true;
        else
            return false;
    }
}
