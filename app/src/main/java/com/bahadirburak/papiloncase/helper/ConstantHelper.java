package com.bahadirburak.papiloncase.helper;

public class ConstantHelper {


    public static final String SERVER_ADDRESS = "http://10.0.2.2:5000/";
    //public static final String SERVER_ADDRESS = "http://192.168.1.42:5000/"; if test from mobile device use this url, given your ip address

    public static final int apiTimeout = 30; // second


    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String PAPILON_SHARED_PREF = "PAPILON_SHARED_PREF";


    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 100 meters
    public static final long MIN_TIME_BW_UPDATES = 1; // 1 minute



    public static final double CURRENT_LOCATION_LATITUDE = 39.900162;
    public static final double CURRENT_LOCATION_LONGITUDE = 32.770761;


}
