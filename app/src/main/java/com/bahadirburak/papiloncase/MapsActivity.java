package com.bahadirburak.papiloncase;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import okhttp3.internal.Util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.bahadirburak.papiloncase.data.location.LocationModel;
import com.bahadirburak.papiloncase.data.permission.PermissionModel;
import com.bahadirburak.papiloncase.helper.ConstantHelper;
import com.bahadirburak.papiloncase.helper.LocationHelper;
import com.bahadirburak.papiloncase.helper.PreferenceHelper;
import com.bahadirburak.papiloncase.helper.UtilHelper;
import com.bahadirburak.papiloncase.viewmodel.LocationViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.bahadirburak.papiloncase.databinding.ActivityMapsBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static com.bahadirburak.papiloncase.helper.ConstantHelper.MIN_DISTANCE_CHANGE_FOR_UPDATES;
import static com.bahadirburak.papiloncase.helper.ConstantHelper.MIN_TIME_BW_UPDATES;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private LocationViewModel viewModel;

    private static final int PERMISSION_REQUEST_CODE = 10023;
    private final String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };


    private LocationManager mLocationManager;
    private List<Marker> markerList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        AppManager.setServices();
        AppManager.setAppContext(this);
        viewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        setContentView(binding.getRoot());

        if (!checkPermissions()) {
            ActivityCompat.requestPermissions(this,
                    PERMISSIONS,
                    PERMISSION_REQUEST_CODE);
        }
        fetchLocationFromRemote();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkFechDataFromLocale();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        addCurrentLocationMarker();
    }

    private void addCurrentLocationMarker() {

        Location currentLocation = new Location("");
        currentLocation.setLatitude(ConstantHelper.CURRENT_LOCATION_LATITUDE);
        currentLocation.setLongitude(ConstantHelper.CURRENT_LOCATION_LONGITUDE);
        LatLng myLocation = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        Marker mMapMarker = mMap.addMarker(new MarkerOptions().position(myLocation).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
        markerList.add(mMapMarker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
    }


    private void fetchLocationFromRemote() {
        viewModel.fetchLocationFromRemote().observe(this, dataEntries -> {
            if (dataEntries != null && dataEntries.size() > 0) {
                for (LocationModel locationModel : dataEntries) {
                    Location location = new Location("");
                    location.setLatitude(Double.valueOf(locationModel.latitude));
                    location.setLongitude(Double.valueOf(locationModel.longitude));
                    addMarker(location);
                }
            }
        });
    }

    private void checkFechDataFromLocale() {
        viewModel.fetchLocationFromLocale().observe(this, dataEntries -> {
            if (dataEntries != null && dataEntries.size() > 0) {
                if (UtilHelper.checkNetwork(MapsActivity.this))
                    saveCachedLocations(dataEntries);
            }
        });
    }

    private void saveCachedLocations(List<LocationModel> locationModels) {
        viewModel.saveCachedLocations(locationModels).observe(this, dataEntries -> {
            if (dataEntries != null && dataEntries.isSuccess) {
                viewModel.removeCachedLocations();
            }
        });
    }


    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                for (String permission : PERMISSIONS) {
                    if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                        insertPermissionDeny(permission);
                        return;
                    }
                }
                break;
        }
    }

    private boolean checkPermissions() {
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, mLocationListener);
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, mLocationListener);
            }
        }
        return true;
    }

    private void addMarker(Location location) {
        if (location != null) {
            LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
            Marker mMapMarker = mMap.addMarker(new MarkerOptions().position(myLocation));
            markerList.add(mMapMarker);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markerList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 30;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.animateCamera(cu);
        }
    }

    private void insertLocationData(LocationModel locationModel) {
        viewModel.saveLocationFromRemote(locationModel).observe(this, dataEntries -> {
            if (dataEntries == null || !dataEntries.isSuccess) {
                viewModel.saveLocationFromLocale(locationModel);
            }
        });
    }

    private void insertPermissionDeny(String permission) {
        viewModel.savePermissionFromRemote(new PermissionModel(new Random().nextInt(), permission, UtilHelper.getDate())).observe(this, dataEntries -> {
            if (dataEntries != null) {
            }
        });
    }

    public final LocationListener mLocationListener = new LocationListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onLocationChanged(final Location location) {
            if (!LocationHelper.checkLocationForEvent(location)) {
                addMarker(location);
                LocationModel locationModel = new LocationModel(new Random().nextInt(), String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), UtilHelper.getDate());
                insertLocationData(locationModel);
                new AlertDialog.Builder(MapsActivity.this)
                        .setTitle(R.string.area_error_title)
                        .setMessage(R.string.area_error)
                        .setPositiveButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss())
                        .create()
                        .show();
            }
            LocationHelper.updateLastLocation(location);

        }
    };
}