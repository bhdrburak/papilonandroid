package com.bahadirburak.papiloncase.data.permission;

import android.content.Context;

import com.bahadirburak.papiloncase.data.location.LocationDao;
import com.bahadirburak.papiloncase.data.location.LocationModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {PermissionModel.class}, version = 1, exportSchema = false)
public abstract class PermissionDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static PermissionDatabase getDatabase(Context context) {
        return Room.databaseBuilder(context, PermissionDatabase.class, "permission_database").build();
    }

    public abstract PermissionDao permissionDao();


}