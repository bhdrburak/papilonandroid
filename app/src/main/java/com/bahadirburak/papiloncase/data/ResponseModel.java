package com.bahadirburak.papiloncase.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel {
    @SerializedName("isSuccess")
    @Expose
    public boolean isSuccess;
    @SerializedName("message")
    @Expose
    public String message;
}
