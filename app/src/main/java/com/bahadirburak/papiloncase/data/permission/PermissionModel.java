package com.bahadirburak.papiloncase.data.permission;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "permission_table")
public class PermissionModel {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    public int id;

    @ColumnInfo(name = "permission_type")
    @SerializedName("permissionType")
    @Expose
    public String permissionType;

    @ColumnInfo(name = "date")
    @SerializedName("date")
    @Expose
    public String date;

    public PermissionModel(int id, String permissionType, String date) {
        this.id = id;
        this.permissionType = permissionType;
        this.date = date;
    }
}
