package com.bahadirburak.papiloncase.data.location;

import android.content.Context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {LocationModel.class}, version = 1, exportSchema = false)
public abstract class LocationDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static LocationDatabase getDatabase(Context context) {
        return Room.databaseBuilder(context, LocationDatabase.class, "location_database").build();
    }

    public abstract LocationDao locationDao();


}
