package com.bahadirburak.papiloncase.data.location;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface LocationDao {

    @Query("SELECT * FROM location_table")
    LiveData<List<LocationModel>> getAllLocation();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(LocationModel locationModel);


    @Query("DELETE FROM location_table")
    void delete();



}
