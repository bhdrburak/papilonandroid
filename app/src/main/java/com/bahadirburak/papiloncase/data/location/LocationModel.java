package com.bahadirburak.papiloncase.data.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "location_table")
public class LocationModel {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    public int id;

    @ColumnInfo(name = "latitude_info")
    @SerializedName("latitude")
    @Expose
    public String latitude;

    @ColumnInfo(name = "longitude_info")
    @SerializedName("longitude")
    @Expose
    public String longitude;

    @ColumnInfo(name = "location_date")
    @SerializedName("date")
    @Expose
    public String date;

    public LocationModel(int id, String latitude, String longitude, String date) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
    }
}
