package com.bahadirburak.papiloncase.data.permission;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PermissionDao {

    @Query("SELECT * FROM permission_table")
    LiveData<List<PermissionModel>> getAllPermission();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PermissionModel permissionModel);

    @Query("DELETE FROM permission_table")
    void delete();

}
