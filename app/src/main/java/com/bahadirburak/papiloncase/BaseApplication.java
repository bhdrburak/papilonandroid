package com.bahadirburak.papiloncase;

import android.app.Application;
import android.content.Context;


import androidx.lifecycle.LifecycleObserver;

public class BaseApplication  extends Application implements LifecycleObserver {

    private static BaseApplication application;

    public static Context getContext() {
        return application.getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

    }

}
