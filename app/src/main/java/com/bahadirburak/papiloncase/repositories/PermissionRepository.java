package com.bahadirburak.papiloncase.repositories;

import android.content.Context;
import android.util.Log;

import com.bahadirburak.papiloncase.AppManager;
import com.bahadirburak.papiloncase.data.ResponseModel;
import com.bahadirburak.papiloncase.data.location.LocationDatabase;
import com.bahadirburak.papiloncase.data.location.LocationModel;
import com.bahadirburak.papiloncase.data.permission.PermissionDao;
import com.bahadirburak.papiloncase.data.permission.PermissionDatabase;
import com.bahadirburak.papiloncase.data.permission.PermissionModel;
import com.bahadirburak.papiloncase.webService.ResponseCall;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;

public class PermissionRepository {
    private Context mContext;
    private final PermissionDatabase permissionDatabase;
    private final PermissionDao permissionDao;


    public static PermissionRepository getInstance(Context context) {
        return new PermissionRepository(context);
    }

    private PermissionRepository(Context context) {
        mContext = context;
        this.permissionDatabase = PermissionDatabase.getDatabase(context);
        this.permissionDao = permissionDatabase.permissionDao();
    }



    public LiveData<List<PermissionModel>> fetchLocationFromLocale(){
        LiveData<List<PermissionModel>> data = permissionDao.getAllPermission();
        permissionDatabase.close();
        return data;
    }

    public MutableLiveData<ResponseModel> savePermissionFromRemote(PermissionModel permissionModel) {
        MutableLiveData<ResponseModel> data = new MutableLiveData<>();
        ResponseCall<ResponseModel> callback = new ResponseCall<>(mContext, data);
        Call<ResponseModel> call = AppManager.retrofitService.addPermission(permissionModel);
        call.enqueue(callback);
        return data;
    }

    public void savePermissionFromLocale(PermissionModel permissionModel){
        LocationDatabase.databaseWriteExecutor.execute(() -> {
            permissionDao.insert(permissionModel);
            permissionDatabase.close();
        });
    }

    public void removeFromLocale(){
        LocationDatabase.databaseWriteExecutor.execute(() -> {
            permissionDao.delete();
            permissionDatabase.close();
        });
    }
}
