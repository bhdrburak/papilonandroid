package com.bahadirburak.papiloncase.repositories;

import android.content.Context;
import android.util.Log;

import com.bahadirburak.papiloncase.AppManager;
import com.bahadirburak.papiloncase.data.ResponseModel;
import com.bahadirburak.papiloncase.data.location.LocationDao;
import com.bahadirburak.papiloncase.data.location.LocationDatabase;
import com.bahadirburak.papiloncase.data.location.LocationModel;
import com.bahadirburak.papiloncase.webService.ResponseCall;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;

public class LocationRepository {

    private Context mContext;
    private final LocationDatabase locationDatabase;
    private final LocationDao locationDao;


    public static LocationRepository getInstance(Context context) {
        return new LocationRepository(context);
    }

    private LocationRepository(Context context) {
        mContext = context;
        this.locationDatabase = LocationDatabase.getDatabase(context);
        locationDao = locationDatabase.locationDao();
    }


    public MutableLiveData<List<LocationModel>> fetchLocationFromRemote() {
        MutableLiveData<List<LocationModel>> data = new MutableLiveData<>();
        ResponseCall<List<LocationModel>> callback = new ResponseCall<>(mContext, data);
        Call<List<LocationModel>> call = AppManager.retrofitService.getAllLocations();
        call.enqueue(callback);
        Log.d("TAG", "initHandShake: ");
        return data;
    }


    public LiveData<List<LocationModel>> fetchLocationFromLocale(){
        LiveData<List<LocationModel>> data = locationDao.getAllLocation();
        locationDatabase.close();
        return data;
    }

    public MutableLiveData<ResponseModel> saveLocationFromRemote(LocationModel locationModel) {
        MutableLiveData<ResponseModel> data = new MutableLiveData<>();
        ResponseCall<ResponseModel> callback = new ResponseCall<>(mContext, data);
        Call<ResponseModel> call = AppManager.retrofitService.addLocation(locationModel);
        call.enqueue(callback);
        return data;
    }


    public MutableLiveData<ResponseModel> saveCachedLocations(List<LocationModel> locationModels) {
        MutableLiveData<ResponseModel> data = new MutableLiveData<>();
        ResponseCall<ResponseModel> callback = new ResponseCall<>(mContext, data);
        Call<ResponseModel> call = AppManager.retrofitService.addCachedLocations(locationModels);
        call.enqueue(callback);
        return data;
    }

    public void saveLocationFromLocale(LocationModel locationModel){
        LocationDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                locationDao.insert(locationModel);
                locationDatabase.close();
            }
        });
    }

    public void removeFromLocale(){
        LocationDatabase.databaseWriteExecutor.execute(() -> {
            locationDao.delete();
            locationDatabase.close();
        });
    }

}
