package com.bahadirburak.papiloncase.webService;

import com.bahadirburak.papiloncase.data.ResponseModel;
import com.bahadirburak.papiloncase.data.location.LocationModel;
import com.bahadirburak.papiloncase.data.permission.PermissionModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitService {

    @GET("location")
    Call<List<LocationModel>> getAllLocations();


    @POST("location")
    Call<ResponseModel> addLocation(@Body LocationModel locationModel);


    @POST("addCachedlocations")
    Call<ResponseModel> addCachedLocations(@Body List<LocationModel> locationModels);


    @GET("permission")
    Call<List<PermissionModel>> getAllPermission();


    @POST("permission")
    Call<ResponseModel> addPermission(@Body PermissionModel permissionModel);


}
