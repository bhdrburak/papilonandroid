package com.bahadirburak.papiloncase.webService;

import android.content.Context;


import com.bahadirburak.papiloncase.AppManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class ResponseCall<T> implements Callback<T> {

    private final Context context;
    private final MutableLiveData<T> data;
    private Call<T> call;

    public ResponseCall(Context context, MutableLiveData<T> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, Response<T> response) {
        this.call = call;

        if (response.isSuccessful()) {
            data.setValue(response.body());
            AppManager.appRequestAttempt = 0;
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        this.call = call;
        if (t.getLocalizedMessage() != null && t.getLocalizedMessage().contains("Canceled"))
            ResponseHandler.setErrorType(ResponseHandler.ErrorType.cancel);
        else
            ResponseHandler.setError(context, null);

        data.setValue(null);
    }



}