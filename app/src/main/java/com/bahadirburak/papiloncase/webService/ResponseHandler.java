package com.bahadirburak.papiloncase.webService;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.bahadirburak.papiloncase.AppManager;
import com.bahadirburak.papiloncase.R;
import com.bahadirburak.papiloncase.helper.UtilHelper;
import com.google.gson.Gson;
import org.json.JSONObject;

import okhttp3.ResponseBody;

public final class ResponseHandler {

    private static String errorMessage;
    private static ErrorType type;

    public static void setError(Context context, ResponseBody error) {
        if (UtilHelper.checkNetwork(context)) {
            try {
                JSONObject errorJson = new JSONObject(error.string());
                ErrorModel errorModel = new Gson().fromJson(errorJson.toString(), ErrorModel.class);

                if (errorModel.errorCode != null) {
                    errorMessage = errorModel.message;
                }

                type = ErrorType.normal;
            } catch (Exception e) {
                errorMessage = context.getResources().getString(R.string.error_try_again);
                type = ErrorType.other;
            }
        } else {
            type = ErrorType.network;
            errorMessage = context.getResources().getString(R.string.check_network_connection);
            AlertDialog.Builder builder = new AlertDialog.Builder(AppManager.appContext);
            builder.setTitle(R.string.case_study);
            builder.setMessage(errorMessage);
            builder.setPositiveButton(R.string.close, (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        }
    }

    public static void setErrorType(ErrorType type) {
        ResponseHandler.type = type;
    }


    public enum ErrorType {
        unAuthorized,
        normal,
        network,
        signOut,
        other,
        cancel
    }

}

