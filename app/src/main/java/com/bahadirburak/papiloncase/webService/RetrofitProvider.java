package com.bahadirburak.papiloncase.webService;

import com.bahadirburak.papiloncase.helper.ConstantHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bahadirburak.papiloncase.helper.ConstantHelper.apiTimeout;


public class RetrofitProvider {

    private static OkHttpClient vHttpClient;

    public static Retrofit createClient() {
        String baseUrl;
        baseUrl = ConstantHelper.SERVER_ADDRESS;


        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request.Builder ongoing = chain.request().newBuilder();
                    return chain.proceed(ongoing.build());
                })
                .readTimeout(apiTimeout, TimeUnit.SECONDS)
                .connectTimeout(apiTimeout, TimeUnit.SECONDS)
                .writeTimeout(apiTimeout, TimeUnit.SECONDS);




        vHttpClient = httpClientBuilder.build();

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(vHttpClient)
                .build();
    }

    public static void cancel() {
        if (vHttpClient != null)
            vHttpClient.dispatcher().cancelAll();
    }

    public static <T> T createService(Class<T> genericClass) {
        Retrofit retrofit = createClient();
        return retrofit.create(genericClass);
    }

}
