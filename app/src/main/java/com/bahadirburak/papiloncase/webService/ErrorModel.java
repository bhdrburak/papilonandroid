package com.bahadirburak.papiloncase.webService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorModel {

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("errorCode")
    @Expose
    public Integer errorCode;

}