package com.bahadirburak.papiloncase.viewmodel;

import android.app.Application;

import com.bahadirburak.papiloncase.data.ResponseModel;
import com.bahadirburak.papiloncase.data.location.LocationModel;
import com.bahadirburak.papiloncase.data.permission.PermissionModel;
import com.bahadirburak.papiloncase.repositories.LocationRepository;
import com.bahadirburak.papiloncase.repositories.PermissionRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class LocationViewModel extends AndroidViewModel {
    private LocationRepository repository;
    private PermissionRepository permissionRepository;

    public LocationViewModel(@NonNull Application application) {
        super(application);
        this.repository = LocationRepository.getInstance(application);
        this.permissionRepository = PermissionRepository.getInstance(application);
    }

    public MutableLiveData<List<LocationModel>> fetchLocationFromRemote() {
        return repository.fetchLocationFromRemote();
    }


    public LiveData<List<LocationModel>> fetchLocationFromLocale() {
        return repository.fetchLocationFromLocale();
    }


    public MutableLiveData<ResponseModel> saveCachedLocations(List<LocationModel> locationModels) {
        return repository.saveCachedLocations(locationModels);
    }


    public void removeCachedLocations() {
        repository.removeFromLocale();
    }

    public void saveLocationFromLocale(LocationModel locationModel) {
        repository.saveLocationFromLocale(locationModel);
    }


    public MutableLiveData<ResponseModel> saveLocationFromRemote(LocationModel locationModel) {
        return repository.saveLocationFromRemote(locationModel);
    }



    public MutableLiveData<ResponseModel> savePermissionFromRemote(PermissionModel permissionModel) {
        return permissionRepository.savePermissionFromRemote(permissionModel);
    }

}
